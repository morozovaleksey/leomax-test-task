import { CART_ADD_COUNT,CART_DELETE_PRODUCT, CART_ADD_PROMOCODE} from '../config/constants';

export function removeProductById (productId) {
  return (dispatch, getState) => {
    dispatch({
      type: CART_DELETE_PRODUCT,
      payload: {
        id: productId
      }
    });
  }
}

export function changeCount (productId,sign) {
  return (dispatch, getState) => {
    dispatch({
      type: CART_ADD_COUNT,
      payload: {
        id: productId,
        sign: sign
      }
    });
  }
}

export function addPromoCode (promocode) {
    return (dispatch, getState) => {
        dispatch({
            type: CART_ADD_PROMOCODE,
            payload: {
                promocode: promocode
            }
        });
    }
}
