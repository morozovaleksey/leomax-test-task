import React, { Component } from 'react';
import Header from './Header';
import CartList from './CartList';
import Footer from './Footer';
import '../assets/scss/fonts.scss';
import '../assets/scss/App.scss';

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header/>
        <div className="content">
          <CartList/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
