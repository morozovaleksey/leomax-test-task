import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from '../assets/images/shop-log.png';

class Header extends Component {
  render() {
    let {products} = this.props.cart;
    let countProducts = 0;
    products.map(function (product) {
        countProducts +=product.count;
    });

    return (
      <header >
        <div className="bg-color-grey header-top">
          <div className="container">
            <div className="header">
              <div className="header__phone-block">
                <div className="header__phone"> 8 (800) 500-75-55</div>
                <span className="header__phone-text">Бесплатный звонок по России</span>
              </div>
              <div className="header-right-block">
                <a href="" className="header__star-block header-right-block__elem">
                  <span className="icon-star header__icon"></span>
                  <span className="header__icon-text">Звездный блог</span>
                </a>
                <a href="" className="header__youtube-block header-right-block__elem">
                  <span className="icon-youtube header__icon"></span>
                  <span className="header__icon-text">Смотрите нас на YouTube</span>
                </a>
                <a href="" className="header__live-block header-right-block__elem">
                  <span className="icon-live header__icon"></span>
                  <span className="header__icon-text">Смотрите наш прямой эфир</span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="header-main ">
            <div className="header-main__left">
              <div className="header-main__logo">
                <a href="#" className="header-main__logo-link">
                  <img src={logo}/>
                  <span className="header-main__logo-text">New Look</span>
                </a>
              </div>
              <div className="header-main__search">
                <input className="" type="text" placeholder="Поиск по сайту"/>
                <button className="header-main__search-button"></button>
              </div>
            </div>
            <div className="header-main__right">
              <div className="header-main__avatar">
                <span className="header-main__avatar-icon"></span>
                <span className="header-main__avatar-name">Анастасия</span>
              </div>
              <div className="header-main__cart">
                <span className="header-main__cart-icon"></span>
                <span className="header-main__cart-text">В корзине:<br />{countProducts} товара </span>
              </div>
            </div>
          </div>
        </div>

        <div className="header-category-menu bg-color-light-grey">
          <div className="container">
            <div className="mobile-menu">
              <input className="burger-check" id="burger-check" type="checkbox"/><label htmlFor="burger-check"  className="burger"></label>
              <nav id="navigation1" className="navigation">
                <ul className="mobile-header-category-list">
                  <li>
                    <a href="#" className="active">Одежда и аксессуары</a>
                  </li>
                  <li>
                    <a href="#">Обувь</a>
                  </li>
                  <li>
                    <a href="#">Украшения</a>
                  </li>
                  <li>
                    <a href="#">Красота и здоровье</a>
                  </li>
                  <li>
                    <a href="#">Товары для дома</a>
                  </li>
                  <li>
                    <a href="#">Товары для кухни</a>
                  </li>
                </ul>
              </nav>
            </div>
            <ul className="header-category-list">
              <li>
                <a href="#" className="active">Одежда и аксессуары</a>
              </li>
              <li>
                <a href="#">Обувь</a>
              </li>
              <li>
                <a href="#">Украшения</a>
              </li>
              <li>
                <a href="#">Красота и здоровье</a>
              </li>
              <li>
                <a href="#">Товары для дома</a>
              </li>
              <li>
                <a href="#">Товары для кухни</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="header-category-menu-child">
          <div className="container">
            <ul className="header-category-list header-category-list-child">
              <li>
                <a href="">Женская одежда</a>
              </li>
              <li>
                <a href="">Мужская одежда</a>
              </li>
              <li>
                <a href="">Аксессуары</a>
              </li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

function mapStateToProps ( state ) {
  return {
    cart: state.cart,
  }
}

const mapDispatchToProps = {

};
export default connect ( mapStateToProps, mapDispatchToProps ) ( Header );