import React from 'react';

export default class Image extends React.Component {
  render() {
    const { name } = this.props;

    return <img src = { name } />
  }
}