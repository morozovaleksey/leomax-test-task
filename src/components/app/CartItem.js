import React, { Component } from 'react';
import { connect } from 'react-redux';
import {removeProductById}  from '../../action/cart';
import {changeCount}  from '../../action/cart';
import Image from '../Image';

class CartItem extends Component {
  constructor ( props ) {
    super ( props );
  }

  render() {
    const product = this.props.product;
    const {image} = product;
    return (
      <div className="table cart-list-subjects__content">
        <div className="td td-good"><a href="#">
          <Image name = { image }/>
        </a></div>
        <div className="td td-desc">
          <p className="name">{product.desc}</p>
          <p className="code">Код: {product.code}</p>
          <p className="size">Размер: {product.size}</p>
          <p className="color">Цвет: {product.color}</p>
        </div>
        <div className="td td-count">
          <div className="number-block">
            <button className="minus" onClick={(e) => this.props.changeCount(product.id,"-")}>-</button>
            <span className="number">{product.count}</span>
            <button className="plus" onClick={() => this.props.changeCount(product.id,"+")}>+</button>
          </div>
        </div>
        <div className="td td-price cart-list-subjects__price">{product.price} руб.</div>
        <div className="td td-delete"><button className="delete"  onClick={() => this.props.removeProductById(product.id)}></button></div>
      </div>
    )
  }
}

function mapStateToProps ( state ) {
  return {
      cart: state.cart,
  }
}

const mapDispatchToProps = {
  changeCount,
  removeProductById
};
export default connect ( mapStateToProps, mapDispatchToProps ) ( CartItem );
