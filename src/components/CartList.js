import React, { Component } from 'react';
import { connect } from 'react-redux';
import CartItem from '../components/app/CartItem';
import {addPromoCode}  from '../action/cart';

import goodsAdditional from '../assets/images/good-additional1.jpg';

class CartList extends Component {

  handleAddPromocode() {
    let promocodeValue = document.getElementById("promocode").value;
    this.props.addPromoCode(promocodeValue);
  }

  render() {
    const {products} = this.props.cart;
    const {promocode} = this.props.cart;
    let totalSum = 0;
    let finalSum = 0;
    let promocodePrice = 0;
    products.map(function(product) {
      totalSum += product.count * product.price;
    });
    if(promocode == "123456") {
        promocodePrice = 1800;
        finalSum = totalSum - 1800;
    } else {
        finalSum = totalSum;
    }




    return (
      <div className="container">
        <div className="cart-block-title-wrapper">
          <hr className="line" />
          <span className="cart-block-title"> Ваша корзина </span>
        </div>
        <div className="cart-list-subjects">

          <div className="table cart-list-subjects__head table cart-list-subjects__content">
            <div className="td td-good">Товар</div>
            <div className="td td-desc">Описание</div>
            <div className="td td-count">Количество</div>
            <div className="td td-price">Цена</div>
            <div className="td td-delete">Удалить</div>
          </div>
          <div id="cart-list-item">
            {
              products.map ( ( product, i ) =>
                <CartItem key = {product.id} product={product}/>
              )
            }
          </div>
        </div>
        <div className="total-block">
          <div className="total-left">
            <div className="promocode">
              <div className="promocode-text">Есть промокод?</div>
              <div className="promocode-input-wrapper">
                <input className="promocode-input" id="promocode" type="text" placeholder="Введите промокод" />
                <button className="btn btn-grey" onClick={() => this.handleAddPromocode()} >Применить</button>
              </div>
            </div>
          </div>
          <div className="total-right">
            <div className="total-item-wrapper">
              <label>Сумма заказа:</label>
              <span id="total-sum">{totalSum} руб.</span>
            </div>
            <div className="total-item-wrapper total-item-wrapper-orange">
              <label>Промокод:</label>
              <span>-{promocodePrice} руб.</span>
            </div>
            <div className="total-item-wrapper total-item-wrapper-final">
              <label>Всего:</label>
              <span id="final-sum">{finalSum} руб.</span>
            </div>
            <div className="total-item-wrapper total-item-wrapper__order-button-wrapper">
              <button className="btn btn-orange total-item-wrapper__order-button">Оформить заказ</button>
            </div>
          </div>
        </div>
        <div className="cart-block-title-wrapper">
          <hr className="line" />
          <span className="cart-block-title"> Добавьте к вашему заказу </span>
        </div>
        <div className="additional-subjects">
          <a href="#" className="additional-subject">
            <img className="additional-subject__img" src={goodsAdditional}/>
            <div className="additional-subject__name">Солнечные очки зеленого цвета в стиле ретро</div>
            <div className="additional-subject__price">2 450 руб.</div>
          </a>
          <a href="#" className="additional-subject">
            <img className="additional-subject__img" src={goodsAdditional}/>
            <div className="additional-subject__name">Солнечные очки зеленого цвета в стиле ретро</div>
            <div className="additional-subject__price">2 450 руб.</div>
          </a>
          <a href="#" className="additional-subject">
            <img className="additional-subject__img" src={goodsAdditional}/>
            <div className="additional-subject__name">Солнечные очки зеленого цвета в стиле ретро</div>
            <div className="additional-subject__price">2 450 руб.</div>
          </a>
          <a href="#" className="additional-subject">
            <img className="additional-subject__img" src={goodsAdditional}/>
            <div className="additional-subject__name">Солнечные очки зеленого цвета в стиле ретро</div>
            <div className="additional-subject__price">2 450 руб.</div>
          </a>
        </div>
      </div>
    );
  }
}

function mapStateToProps ( state ) {
  return {
    cart: state.cart,
  }
}

const mapDispatchToProps = {
    addPromoCode
};
export default connect ( mapStateToProps, mapDispatchToProps ) ( CartList );