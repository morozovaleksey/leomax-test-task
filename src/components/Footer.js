import React, { Component } from 'react';
import footerLogo from '../assets/images/footer-logo.png';

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="container">
          <div className="social-block">
            <div className="social-block__text"> SHOP24 в социальных сетях:</div>
            <div className="social-block__link-wrapper">
              <a href="#" className="social-block__link">
                <span className="icon vk-icon"></span>
              </a>
              <a href="#"> <span className="icon facebook-icon"></span></a>
              <a href="#"> <span className="icon odnoklassniki-icon"></span></a>
              <a href="#"><span className="icon pinterest-icon"></span></a>
              <a href="#"><span className="icon heart-icon"></span></a>
              <a href="#"><span className="icon instagram-icon"></span></a>
              <a href="#"><span className="icon youtube-icon"></span></a>
            </div>
          </div>
        </div>
        <hr className="footer__line"/>
        <div className="container">
          <div className="footer__info">
            <div className="footer__left">
              <div className="footer__logo">
                <img src={footerLogo} />
              </div>
              <div className="footer__phone">
                <p className="phone">8 (800) 500-75-55*</p>
                <p className="phone-desc">*Бесплатный звонок по всей России</p>
                <p className="phone">8 (495) 733-96-03</p>
              </div>
            </div>
            <div className="footer__right">
              <div className="pages-block">
                <div className="page-block">
                  <div className="page-block__title">Каталог товаров</div>
                  <ul>
                    <li><a href="">Одежда и аксесуары</a></li>
                    <li><a href="">Обувь</a></li>
                    <li><a href="">Украшения</a></li>
                    <li><a href="">Красота и здоровье</a></li>
                    <li><a href="">Товары для дома и отдыха</a></li>
                    <li><a href="">Товары для кухни</a></li>
                  </ul>
                </div>
                <div className="page-block">
                  <div className="page-block__title">Заказ</div>
                  <ul>
                    <li><a href="">Оплата и доставка</a></li>
                    <li><a href="">Возврат</a></li>
                    <li><a href="">Помощь</a></li>
                    <li><a href="">Благотворительный Фонд Константина Хабенского</a></li>
                    <li><a href="">Гарантия на дополнительное обслуживание</a></li>
                    <li><a href="">Пользовательское соглашение</a></li>
                  </ul>
                </div>
                <div className="page-block">
                  <div className="page-block__title">Shop24</div>
                  <ul>
                    <li><a href="">Смотреть прямой эфир</a></li>
                    <li><a href="">Расписание передач</a></li>
                    <li><a href="">Акции</a></li>
                    <li><a href="">Личный кабинет</a></li>
                    <li><a href="">Поиск и карта сайта</a></li>
                    <li><a href="">Карта брендов</a></li>
                    <li><a href="">Обратная связь</a></li>
                  </ul>
                </div>
                <div className="page-block">
                  <div className="page-block__title">Информация</div>
                  <ul>
                    <li><a href="">О канале</a></li>
                    <li><a href="">Сотрудничество</a></li>
                    <li><a href="">Покупайте с нами</a></li>
                    <li><a href="">Контакты</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className="footer__line"/>
        <div className="container">
          <div className="copyright-block">
            <p>Copyright © Товар ООО «Стиль и Мода», 2014-2016. Все права защищены.</p>
            <p>При использовании материалов сайта ссылка на www.shop24.com обязательна.</p>
            <p>Телефон  8 800 500-75-55</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;