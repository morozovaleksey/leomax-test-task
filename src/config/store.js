import { createStore, applyMiddleware } from 'redux'
import reducer from '../reducers'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';


function configureStore() {
  let store = createStore(reducer, {}, composeWithDevTools(applyMiddleware(thunk)));
  return store
}

const store = configureStore();

export default store;