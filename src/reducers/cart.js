import { CART_ADD_COUNT,CART_DELETE_PRODUCT,CART_ADD_PROMOCODE} from '../config/constants';
import { Record, OrderedMap } from 'immutable'
import {goods} from '../api/products';
const DefaultReducerState = Record(
  {
      products: goods,
      promocode: null
  }
);

export default function dataReducer (state = new DefaultReducerState({}), action) {

  const { type, response, error, payload } = action;

  switch (type) {
    case CART_DELETE_PRODUCT:
      const {id : removeId} = payload;
      return state.update('products', products => {
        return products.filter(element => {
          return element.id != removeId
        })
      });

      case CART_ADD_COUNT:
          const {id : productId,sign: sign} = payload;
          return state.update('products', products => {
              return products.filter(element => {

                if(element.id == productId) {
                    if(sign == "+") {
                        element.count = element.count+1;
                    } else {
                        if(element.count == 0) {
                            element.count = element.count;
                        } else {
                            element.count = element.count-1;
                        }
                    }
                }

                return element;
              })
          });

      case CART_ADD_PROMOCODE:
          const promocodeInput = payload.promocode;
          return state.update('promocode', promocode => {
            return promocodeInput;
          });

      default:
      return state
  }
}

